# Bonus Challenge 
--

### Pull the image:

##### command:

```bash
docker pull sonarqube
```
----

### Run the image:

##### command:

```bash
docker run -d -p9000:9000 sonarqube
```

##### go to localhost:9000:

```bash
Find the app, login and generate an auth token later to be used by sonar-scanner.

default username / pass: admin/admin

My account -> security -> generate tokens
```
----

### Now, we need to configure sonar-scanner

##### command:

```bash 
docker pull sonarsource/sonar-scanner-cli
```

##### command:

```bash
cd bonus
```
----
##### command:

```bash
docker pull skilldlabs/sonar-scanner
```

```bash
docker run --network host -v $(pwd):/root/src sonarsource/sonar-scanner-cli sonar-scanner \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=9874055f4b3ae0327f7429a8768e402589964aa6 \
  -Dsonar.projectKey=test \
  -Dsonar.projectName=test \
  -Dsonar.projectBaseDir=/root \
  -Dsonar.sources=./src
```

Find the scan results in your sonarqube instance!
The default config scans all the files, see if you can tweak it
to only .py files.

----