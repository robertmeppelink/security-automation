# Challenge 4

This challenge describes how to run our first ZAP-active scan

----

### Run our vulnerable container

##### command:

```bash
docker run -p5000:5000 ssti
```
----

### Pull the ZAP container

##### command:

``` bash
docker pull owasp/zap2docker-stable
```

#### output:

```bash
Using default tag: latest
latest: Pulling from owasp/zap2docker-stable
423ae2b273f4: Pull complete 
de83a2304fa1: Pull complete 
f9a83bce3af0: Pull complete 
b6b53be908de: Pull complete 
dfa4c0ed9f01: Downloading [======>                                            ]  59.13MB/444.5MB
0d0271dc7f26: Download complete 
ba10134fb40f: Download complete 
......... snip ........
```

----

### Start our active-scan on the target

##### command:

Note that $(pwd) is only supported on Linux and MacOS - on Windows you will need to replace this with the full current working directory.

```bash
docker run --network host -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-stable zap-full-scan.py \
    -t http://127.0.0.1:5000 -g gen.conf -r testreport.html
```

##### output:

Check the folder from where you ran the scan to find the "testreport.html" to 
see the output results of the scan!

The scan results can still be upgraded, you need to know ZAP internals to find out how.
Deep dive more into ZAP and see if we can enhance the scan results!
----